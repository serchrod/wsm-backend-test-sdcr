<?php

namespace App\Controller;

use App\Commons\MongoDBClient;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReportsController extends AbstractController
{

    private $mongoClient = null;

    public function __construct()
    {
        $this->mongoClient = new MongoDBClient();
        $this->mongoClient->ConnectDatabase("demo-db");
    }

    /**
     * @Route("/reports", name="retrieve-data", methods={"POST","GET"})
     */
    public function index(Request $request)
    {
        $accountId = $request->get("accountId");
        if($request->get("showAll")){
            $accountId=null;
        }
        $this->mongoClient->UseCollection("accounts");
        $aggregation = $this->accountAggregations($accountId);
        $result=$this->mongoClient->aggregate($aggregation);
        $resultFlat=$this->flatArray($result);


        return $this->render('reports/index.html.twig', [
            'controller_name' => 'ReportsController',
            'data' => $resultFlat,
            'accountId' => $accountId
        ]);
    }



    public function accountAggregations($accountId)
    {
        $aggregation = [
            ['$match' => [
                'status' => 'ACTIVE'
            ]], ['$lookup' => [
                'from' => 'metrics',
                'as' => 'joined',
                'let' => ['id' => '$accountId'],
                'pipeline' => [
                    ['$match' => [
                        '$expr' => [
                            '$eq' => ['$accountId', '$$id']
                        ]
                    ]], ['$group' => [
                        '_id' => '$accountId',
                        'spend' => ['$sum' => '$spend'],
                        'impressions' => ['$sum' => '$impressions'],
                        'clicks' => ['$sum' => '$clicks']
                    ]
                    ], [
                        '$addFields' => [
                            'costPerClick' => ['$divide' => ['$spend', '$clicks']]
                        ]
                    ],
                ]
            ]]
        ];

        if ($accountId != null) {
            array_push($aggregation, [
                    '$match' => [
                        'accountId' => $accountId
                    ]]
            );
        }

        return $aggregation;
    }

    public function flatArray($element){
        $result=array();

        foreach ($element as $row){


                $data = [
                    "accountId" => $row["accountId"],
                    "accountName" => $row["accountName"],
                    "spend" => isset($row["joined"][0]["spend"]) ? $row["joined"][0]["spend"] : 0,
                    "impressions" => isset($row["joined"][0]["impressions"]) ? $row["joined"][0]["impressions"] : 0,
                    "clicks" => isset($row["joined"][0]["clicks"]) ? $row["joined"][0]["clicks"] : 0,
                    "costPerClick" => isset($row["joined"][0]["costPerClick"]) ? round($row["joined"][0]["costPerClick"],2) : 0,
                ];
                array_push($result, $data);
            }

        usort($result, function ($item1, $item2) {
            return $item2['costPerClick'] <=> $item1['costPerClick'];
        });
        return $result;
    }
}
