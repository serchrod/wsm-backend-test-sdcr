<?php

namespace App\Commons;

use MongoDB\Client as MongoClient;

/*Sergio Chamorro
 * I create this class to have an easier handling of the mongo client. Facade pattern
 *  */

class MongoDBClient
{

    private $MongoClient=null;
    private $databaseConnection=null;
    private $CollectionSet=null;

    public function __construct($uri = 'mongodb://127.0.0.1/', array $uriOptions = [], array $driverOptions = [])
    {
        $MongoClient= new MongoClient($uri,$uriOptions,$driverOptions);
        $this->MongoClient=$MongoClient;
    }

    public function ConnectDatabase($databaseName){
        $this->databaseConnection=$this->MongoClient->$databaseName;
    }

    public function UseCollection($collectionName){
        if(!$this->databaseConnection){
            throw $this->DatabaseConnectionNotFoundException("Database Connection not found");
        }

        $this->CollectionSet=$this->databaseConnection->$collectionName;
    }

    public function findOne($filter = [], array $options = []){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
        return $this->CollectionSet->findOne($filter,$options);
    }

    public function find($filter = [], array $options = [],$cursor=false){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
        if($cursor) return $this->CollectionSet->find($filter,$options);

        return $this->CollectionSet->find($filter,$options);
    }

    public function insertOne(array $data){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
         $this->CollectionSet->insertOne($data);
    }

    public function insertMany(array $data){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
            $this->CollectionSet->insertMany($data);
    }

    public function updateOne($filter, $update, array $options = []){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
        $this->CollectionSet->updateOne($filter,$update,$options);
    }

    public function updateMany($filter, $update, array $options = []){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
        $this->CollectionSet->updateMany($filter,$update,$options);
    }

    public function deleteOne($filter, array $options = []){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
        $this->CollectionSet->deleteOne($filter,$options);
    }

    public function deleteMany($filter, array $options = []){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
        $this->CollectionSet->deleteMany($filter,$options);
    }

    public function aggregate(array $pipeline, array $options = []){
        if(!$this->CollectionSet){
            throw $this->CollectionNotInstance("The collection has not been used");
        }
        return $this->CollectionSet->aggregate($pipeline,$options);
    }

    protected function DatabaseConnectionNotFoundException(string $message = 'Not Found', \Throwable $previous = null)
    {
        return new \Exception($message, $previous);
    }

    protected function CollectionNotInstance(string $message = 'Not Found', \Throwable $previous = null)
    {
        return new \Exception($message, $previous);
    }

}
